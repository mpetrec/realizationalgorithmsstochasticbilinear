import form_pow_repr
from numpy import dot as matrixmultiply
from numpy.linalg import lstsq as linear_least_squares
from own_io import own_print
from  numpy import *
import hankel_matrix
import ConfigParser
import Gnuplot
import sys
from abstract_hankel_generate import *
from word_operations import *
import stoch_bilin 

#Set numerical accuracy
#def_precsion = 1e-1#20
#hankel_matrix.def_precision = def_precision

#form_pow_repr.def_precision = def_precision 

def init_module(l_def_precision = 1e-10 ):
	
        def_precision = l_def_precision
	hankel_matrix.def_precision = l_def_precision

	form_pow_repr.def_precision = l_def_precision 
	return 	



#Class of generalized jump markov systems
class JMLS:

	attribute_list = ["discrete_modes", \
	                  "a_matrices", "b_matrices", \
			   "c_matrices", "d_matrices", "p_matrices", "noise_variance", \
			   "state_dimension", "input_dimension",\
			   "output_dimension"]
	
	constructor_attribute_list = ["discrete_modes", \
	                   "a_matrices", "b_matrices", \
			   "c_matrices", "noise_variance", "p_matrices", "d_matrices"]
			   
	def check_object(self):
		return


	def copy(self, bilsys):
		for attrib in BilinStochSystem.attribute_list:
			self.__dict__[attrib] =  bilsys.__dict__[attrib]
	
	def __init__(self, **arguments):
	         #discrete_modes, a_matrices, b_matrices, \
                 #c_matrices, initial_states, is_check=True):
		if arguments.has_key("copy"):
			JMLS.copy(self,\
			  arguments["copy"])
		
		else:
			data = dict()
			if arguments.has_key("config_file"):
				configf = ConfigParser.\
				            ConfigParser()
				
				fp = arguments["config_file"]

				configf.readfp(fp)

				data_list = configf.items(\
				 "JMLS")

				for element in data_list:
					key       = element[0]
					value     = element[1]
					data[key] = eval(value)

			else:
			   	data = arguments
				
			
			for key in \
			 		JMLS.constructor_attribute_list:
			 		self.__dict__[key] = \
				 		 data[key]
				  
			self.state_dimension = self.a_matrices[self.discrete_modes[0]].shape[0]
			self.input_dimension = self.b_matrices[self.discrete_modes[0]].shape[1]
			self.output_dimension = self.c_matrices[self.discrete_modes[0]].shape[0]

			IsDataGood = True
			#Check sizes:
			for mode in self.discrete_modes:
				IsDataGood = IsDataGood and (shape(self.a_matrices[mode]) == (self.state_dimension,self.state_dimension))
				IsDataGood = IsDataGood and (shape(self.b_matrices[mode]) == (self.state_dimension,self.input_dimension))

				IsDataGood = IsDataGood and (shape(self.c_matrices[mode]) == (self.output_dimension,self.state_dimension))
			IsDataGood = IsDataGood and (shape(self.d_matrices) == (self.output_dimension,self.input_dimension))

			if (not IsDataGood):
				print "Ill formed JMLS, quitting"
				sys.exit(-1)
		
		print "State dimension "+ \
		str(self.state_dimension)+\
		" output dimension "+ \
		str(self.output_dimension)+\
		" input dimension "+ \
		str(self.input_dimension)+"\n"
		
		
	#Prints the matrices of the system	
	def ownprint(self, oprecision=10, supp_small=1):
		print "Discrete modes:"+ str(self.discrete_modes) +"\n"
		for mode in self.discrete_modes:
			print "Discrete mode: "+ \
				str(mode) + "\n "+ \
				"A matrix:\n "+ \
				array2string(self.a_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n "+\
				"B matrix:\n "+\
				array2string(self.b_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n "
			print "C matrix:\n "+\
				array2string(self.c_matrices[mode],\
					precision = oprecision, \
				suppress_small=supp_small)

		print	"D matrix:\n "+\
			array2string(self.d_matrices,\
			precision = oprecision, \
			suppress_small=supp_small)\


		print "P matrix:\n"+array2string(self.p_matrices,\
			precision = oprecision, \
			suppress_small=supp_small)\

		return



	def ComputeBilinearStoch(self):
		
		bil_stoch_discrete_modes = []
		bil_state_dimension = len(self.discrete_modes)* self.state_dimension
		bil_a_matrices = dict([])
		bil_b_matrices = dict([])
		bil_noise_variance = dict([])
                bil_c_matrices = zeros((self.output_dimension, bil_state_dimension))
		bil_d_matrices = self.d_matrices
		psigmas = dict([]);

		alphas = dict([]);


		row_block = 0; 
		for mode1 in self.discrete_modes:
			column_block = 0;
			for mode2 in self.discrete_modes:
				bil_stoch_discrete_modes.append((mode1,mode2))
				A = zeros((bil_state_dimension,bil_state_dimension))
				A[row_block*self.state_dimension:(row_block+1)*self.state_dimension,column_block*self.state_dimension:(column_block+1)*self.state_dimension]=self.a_matrices[mode2];
				bil_a_matrices[(mode1,mode2)]=A
				
				K = zeros((bil_state_dimension, self.input_dimension));

				K[row_block*self.state_dimension:(row_block+1)*self.state_dimension,:]=self.b_matrices[mode2]


				bil_b_matrices[(mode1,mode2)]=K
				
				psigmas[(mode1,mode2)]= self.p_matrices[row_block,column_block]

				alphas[(mode1,mode2)] = 1



				bil_noise_variance[(mode1,mode2)]=self.p_matrices[row_block,column_block]*self.noise_variance[mode2]
				column_block = column_block+1


			bil_c_matrices[:,row_block*self.state_dimension:(row_block+1)*self.state_dimension]=self.c_matrices[mode1]
			
			row_block = row_block + 1


		language=[]

		for modefirst in bil_stoch_discrete_modes:
			for modesecond in bil_stoch_discrete_modes:
				if (modefirst[0]==modesecond[1]):
					language.append((modefirst,modesecond))


		bilsys= stoch_bilin.BilinStochSystem(discrete_modes=bil_stoch_discrete_modes,\
	              a_matrices=bil_a_matrices, \
		      b_matrices=bil_b_matrices ,\
		      c_matrices=bil_c_matrices, \
		      d_matrices= bil_d_matrices, \
		      noise_variance = bil_noise_variance, language = language, psigmas=psigmas, alphas=alphas)
		      
		return bilsys

			
		
