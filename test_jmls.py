#!/usr/bin/env python
#import form_pow_repr
from numpy import *
from sets import *
import hankel_matrix
import stoch_bilin 
import JMLS 
import sys,os
import copy
import Gnuplot
from numpy.random import *



is_output = False
if len(sys.argv) < 2:
	print "Usage: python test_bilstoch.py filename\n"
	sys.exit(-1)
else:
	filename = sys.argv[1]

fp=open(filename)

jmls = JMLS.JMLS(config_file = fp )


print "JMLS \n "
jmls.ownprint ()

fp=open(sys.argv[2])
jmls0 = JMLS.JMLS(config_file = fp )
print "JMLS 2 \n "
jmls0.ownprint ()


bilstochsys = jmls.ComputeBilinearStoch()
print "Bilinear stoch from JMLS:"
bilstochsys.ownprint()

print "Is stable " + str(bilstochsys.IsStable()) + " \n"

print "Is observable " + str(bilstochsys.IsObservable()) + " \n"

print "Is controllable " + str(bilstochsys.IsReachable()) + " \n"

print "Representation of bilsys\n"
repr = bilstochsys.ComputeRepresentation()
repr.ownprint()


bilstochsys0 = jmls0.ComputeBilinearStoch()
print "Bilinear stoch from JMLS0:"
bilstochsys0.ownprint()

print "Is stable " + str(bilstochsys0.IsStable()) + " \n"

print "Is observable " + str(bilstochsys0.IsObservable()) + " \n"

print "Is controllable " + str(bilstochsys0.IsReachable()) + " \n"

mrepr=repr.MinimalRepresentation();
print "Minimal representation\n"
mrepr.ownprint()


mbilstoch = bilstochsys.MinimalSystem()

print "Minimal stochastic bilinear system calculated by the minimization algorithm\n"
mbilstoch.ownprint()

#sys.exit(-1)

hankellength = 2
(hankelm1, msize1) = bilstochsys.HankelMatrix(hankellength)

print "Hankel matrix size: " + str(msize1)+"\n"
print "Hankel matrix dimension2: " + str(shape(hankelm1))+" \n"


(hankelm2, msize2) = mbilstoch.HankelMatrix(hankellength)


print "Hankel matrix size: " + str(msize2)+"\n"
print "Hankel matrix dimension2: " + str(shape(hankelm2))+" \n"

print "Difference in Hankel matrices between original and minimized system:"+ str(linalg.norm(hankelm1-hankelm2))+"\n"

scovariance = bilstochsys.ComputeOutputSelfCovariance()

hbilstoch = stoch_bilin.HankelMatrixToStochBilSys(hankelm2, bilstochsys.output_dimension, bilstochsys.discrete_modes, bilstochsys.psigmas, scovariance, bilstochsys.language,bilstochsys.alphas,msize2)

print "Bilinear stochastic system from Hankel matrix"
hbilstoch.ownprint()

#
series_length = 2*mbilstoch.state_dimension-1
#
(Formal1,IndexList1) = bilstochsys.FormalPowerSeries(series_length)
(Formal2,IndexList2) = mbilstoch.FormalPowerSeries(series_length)
(Formal3,IndexList3) = hbilstoch.FormalPowerSeries(series_length)
(Formal4,IndexList4) = bilstochsys0.FormalPowerSeries(series_length)

print "Relative difference in formal power series matrices between original and minimized systems:"+ str(linalg.norm(Formal1-Formal2,2)/linalg.norm(Formal1,2))+"\n"
print "Relative difference in formal power series matrices between original system and the system from Hankel matrix:"+ str(linalg.norm(Formal1-Formal3,2)/linalg.norm(Formal1,2))+"\n"
print "Relative difference in formal power series matrices between original system and the simple minimal system:"+ str(linalg.norm(Formal1-Formal4,2)/linalg.norm(Formal1,2))+"\n"


T=mbilstoch.Isomorphism(hbilstoch)
print "Isomorphism T from minimal to Hankel-based bil. system"+array2string(T)+"\n"


mbilstoch.checkIsomorphism(T,hbilstoch)


T2=bilstochsys0.Isomorphism(mbilstoch)
print "Isomorphism T from simple minimal to computed minimal bil. system"+array2string(T2)+"\n"
bilstochsys0.checkIsomorphism(T2,mbilstoch)

T3=bilstochsys0.Isomorphism(hbilstoch)
print "Isomorphism T from simple minimal to computed  Hankel bil. system"+array2string(T3)+"\n"
bilstochsys0.checkIsomorphism(T3,hbilstoch)




scovariance2 = mbilstoch.ComputeOutputSelfCovariance()
scovariance3 = hbilstoch.ComputeOutputSelfCovariance()
#
max=-1
hmax=-1
for dstate in bilstochsys.discrete_modes:
	merror = linalg.norm(scovariance[dstate]-scovariance2[dstate],2)/linalg.norm(scovariance[dstate])
	herror = linalg.norm(scovariance[dstate]-scovariance3[dstate],2)/linalg.norm(scovariance[dstate])
	
	print "Discrete mode "+str(dstate)+" self covariance original system "+array2string(scovariance[dstate])+\
	      " self covariance minimal system "+array2string(scovariance2[dstate])+\
	      " self covariance minimal system from Hankel matrix"+array2string(scovariance3[dstate])+"\n"

	if (merror > max):
		max = merror

	if (herror > hmax):
		hmax = herror

print "Maximal relative error in the covariances between original and minimized system: "+str(max)+"\n"
print "Maximal relative error in the covariances between original and system from Hankel matrix: "+str(hmax)+"\n"


sys.exit(-1)



input_type = dict([('q1',"DET"),('q2', "GAUSS"), ('q3', "GAUSS")])


(xstate,youtput,u) =  bilstochsys.SimulateSystemGauss(K=10**6, input_type=input_type)
#(xstate,youtput,u) =  bilstochsys.SimulateSystemDiscrete(K=10**7)

print "Compare output distribution of the bilinear system one based on outputs/matrix formulas"
bilstochsys.CheckEqualityOfOutputDistributions(youtput,u, N=bilstochsys.state_dimension*2)

print "Compare output distributions of the minimal bilinear system and the original one based on outputs"
mbilstoch.CheckEqualityOfOutputDistributions(youtput,u, N=bilstochsys.state_dimension*2)

print "Compare output distributions of the  bilinear system computed from the Hankel matrix and the original one based on outputs"
hbilstoch.CheckEqualityOfOutputDistributions(youtput,u, N=bilstochsys.state_dimension*2)


