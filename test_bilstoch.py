#!/usr/bin/env python
#import form_pow_repr
from numpy import *
from sets import *
import hankel_matrix
import stoch_bilin 
import sys,os
import copy
import Gnuplot
from numpy.random import *


# This program was written for testing purposes. It takes one parameter,
# which is the name of a text file containing the description of the
# GBS. In the sequel, this GBS will be refered to as G. The program then does the following:
# -- prints out the system matrices
# -- checks stability of the matrix \sum_{\sigma \in \Sigma} A_{\sigma} \otimes A_{\sigma}, observability, reachability and minimality of G
# -- computes a minimal GBS G1 equivalent to the original GBS G (realizing the same output process
# -- computes the Hankel matrix H_{\Psi,N+1,N} of the formal power series asscociated with the output y of G. This is done using  the
#     minimal GBS  G1. In this case N=2(n-1)+1=2n-1, where n is the dimension of the original GBS. In addition, the
#     covariances T_{\sigma,\sigma}^{y}, \sigma \in \Sigma are also computed from the matrices of G1. 
# -- from H_{\Psi,N+1,N} and T_{\sigma,\sigma}^{y}, \sigma \in \Sigma another minimal GBS G2 is computed using the realization algorithm.
#    The resulting GBS G2 realizes the same output process y as the original GBS.
# --  The formal power series and the covariances of the output processes of G1, G2, and G are computed and they are compared. Ideally, the difference
#      between them should be small.  When comparing the formal power series, the values of the corresponding power series for words of length at most
#      2n are compared, where n is the dimension of G.  In addition, the corresponding hankel matrices are compared as well.   
#      Both the absolute and the relative errors are computed for formal power series, Hankel-matrices and covariances. The covariances T^y_{sigma,sigma}
#      are represented as a dictionary cov, where cov[\sigma] represents T^y_{\sigma,\sigma}
#  -- Finally, the GBS G is used to simulate an output time series y, using SimulateSystemGauss(). Subsequently, the method CheckEqualityOfOutputDistributions()
#     is used to compare the formal power series \Psi_y and covariances T_{\sigma,\sigma}^y, computed based on the system matrices of G,G1,G2 and the time
#     series youtput respectively.

is_output = False
#stoch_bilin.init_module()
if len(sys.argv) < 2:
	print "Usage: python test_bilstoch.py filename\n"
	sys.exit(-1)
else:
	filename = sys.argv[1]

fp=open(filename)

bilstochsys1 = stoch_bilin.BilinStochSystem(config_file = fp )


#print "Bilsys1 \n "
#bilstochsys1.ownprint ()

#print "Representation of bilsys1\n"
#repr = bilstochsys1.ComputeRepresentation()
#repr.ownprint()


#sys.exit(-1)

#print "Is stable " + str(bilstochsys1.IsStable()) + " \n"

#print "Is observable " + str(bilstochsys1.IsObservable()) + " \n"

#print "Is controllable " + str(bilstochsys1.IsReachable()) + " \n"


#scovariance1 = bilstochsys1.ComputeOutputSelfCovariance()

#for dstate in bilstochsys1.discrete_modes:
#	print "Modes: " + str(dstate)+ "covarice: "+ array2string(scovariance1[dstate])+"\n"


#bilstochsys2 = stoch_bilin.ReprToStochBilSys(repr, bilstochsys1.output_dimension, scovariance1, bilstochsys1.psigmas, bilstochsys1.language, bilstochsys1.alphas)

#print "Bilsys2 \n"
#bilstochsys2.ownprint ()



fp=open(sys.argv[2])
bilstochsys = stoch_bilin.BilinStochSystem(config_file = fp )
print "Bilsys \n "
bilstochsys.ownprint ()

print "Representation of bilsys\n"
repr = bilstochsys.ComputeRepresentation()
repr.ownprint()

#sys.exit(-1)

#print "Is stable " + str(bilstochsys.IsStable()) + " \n"

#print "Is observable " + str(bilstochsys.IsObservable()) + " \n"

#print "Is controllable " + str(bilstochsys.IsReachable()) + " \n"


mrepr = repr.MinimalRepresentation();
print "Minimal representation\n"
mrepr.ownprint()

mbilstoch = bilstochsys.MinimalSystem()

print "Minimal stochastic bilinear system calculated by the minimization algorithm\n"
mbilstoch.ownprint()

print "Is stable minimal" + str(mbilstoch.IsStable()) + " \n"




T=bilstochsys1.Isomorphism(mbilstoch)
print "Isomorphism T from minimal to original bil. system"+array2string(T)+"\n"


bilstochsys1.checkIsomorphism(T,mbilstoch)

hankel_size = 2

(hankelm1, msize1) = bilstochsys.HankelMatrix(hankel_size)
(hankelm2, msize2) = mbilstoch.HankelMatrix(hankel_size)


print "Hankel matrix size: " + str(msize2)+"\n"
print "Hankel matrix dimension2: " + str(shape(hankelm2))+" \n"

print "Difference in Hankel matrices between original and minimized system:"+ str(linalg.norm(hankelm1-hankelm2))+"\n"

scovariance = bilstochsys.ComputeOutputSelfCovariance()

hbilstoch = stoch_bilin.HankelMatrixToStochBilSys(hankelm2, bilstochsys.output_dimension, bilstochsys.discrete_modes, bilstochsys.psigmas, scovariance, bilstochsys.language,bilstochsys.alphas,msize2)

print "Bilinear stochastic system from Hankel matrifp=open(sys.argv[2]fp=open(sys.argv[2]))x"
hbilstoch.ownprint()

pow_number = 2
#
(Formal1,IndexList1) = bilstochsys.FormalPowerSeries(2*pow_number)
(Formal2,IndexList2) = mbilstoch.FormalPowerSeries(2*pow_number)
(Formal3,IndexList3) = hbilstoch.FormalPowerSeries(2*pow_number)

print "Relative difference in formal power series matrices between original and minimized systems:"+ str(linalg.norm(Formal1-Formal2,2)/linalg.norm(Formal1,2))+"\n"
print "Relative difference in formal power series matrices between original system and the system from Hankel matrix:"+ str(linalg.norm(Formal1-Formal3,2)/linalg.norm(Formal1,2))+"\n"



T=bilstochsys1.Isomorphism(mbilstoch)
print "Isomorphism T from minimal to original bil. system"+array2string(T)+"\n"


bilstochsys1.checkIsomorphism(T,mbilstoch)

T2=mbilstoch.Isomorphism(hbilstoch)
print "Isomorphism T2 from minimal to Hankel bil. system"+array2string(T2)+"\n"


mbilstoch.checkIsomorphism(T2,hbilstoch)


sys.exit(-1)



scovariance2 = mbilstoch.ComputeOutputSelfCovariance()
scovariance3 = hbilstoch.ComputeOutputSelfCovariance()
#
max=-1
hmax=-1
for dstate in bilstochsys.discrete_modes:
	print "Mode: " + str(dstate)+" covariance of the original system: "+array2string(scovariance[dstate])+\
		" covariance of the minimal system: "+array2string(scovariance2[dstate])+\
		" covariance of the minimal system from Hankel: "+array2string(scovariance3[dstate])+"\n"

	merror = linalg.norm(scovariance[dstate]-scovariance2[dstate],2)/linalg.norm(scovariance[dstate])
	herror = linalg.norm(scovariance[dstate]-scovariance3[dstate],2)/linalg.norm(scovariance[dstate])

	if (merror > max):
		max = merror

	if (herror > hmax):
		hmax = herror

print "Maximal relative error in the covariances between original and minimized system: "+str(max)+"\n"
print "Maximal relative error in the covariances between original and system from Hankel matrix: "+str(hmax)+"\n"


sys.exit(-1)
#sys.exit(-1)



input_type = dict([('q1',"DET"),('q2', "GAUSS"), ('q3', "GAUSS")])


(xstate,youtput,u) =  bilstochsys.SimulateSystemGauss(K=10**6, input_type=input_type)
#(xstate,youtput,u) =  bilstochsys.SimulateSystemDiscrete(K=10**7)

print "Compare output distribution of the bilinear system one based on outputs/matrix formulas"
bilstochsys.CheckEqualityOfOutputDistributions(youtput,u, N=bilstochsys.state_dimension*2)

print "Compare output distributions of the minimal bilinear system and the original one based on outputs"
mbilstoch.CheckEqualityOfOutputDistributions(youtput,u, N=bilstochsys.state_dimension*2)

print "Compare output distributions of the  bilinear system computed from the Hankel matrix and the original one based on outputs"
hbilstoch.CheckEqualityOfOutputDistributions(youtput,u, N=bilstochsys.state_dimension*2)

