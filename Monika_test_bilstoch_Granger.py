#!/usr/bin/env python
#import form_pow_repr
from numpy import *
from sets import *
import hankel_matrix
import stoch_bilin 
import sys,os
import copy
import Gnuplot
from numpy.random import *

is_output = False
if len(sys.argv) < 2:
	print "Usage: python test_bilstoch.py filename\n"
	sys.exit(-1)
else:
	filename = sys.argv[1]

fp=open(filename)

bilstochsys = stoch_bilin.BilinStochSystem(config_file = fp )

#print "Bilsys \n "
#bilstochsys.ownprint ()

print "Is stable " + str(bilstochsys.IsStable()) + " \n"
print "Is observable " + str(bilstochsys.IsObservable()) + " \n"
print "Is controllable " + str(bilstochsys.IsReachable()) + " \n"





(hankelm1, msize1) = bilstochsys.HankelMatrix(bilstochsys.state_dimension)
#hankelm1=hankelm1[0:msize1,0:400]

print "Hankel matrix size: " + str(msize1)+"\n"
print "Hankel matrix dimension2: " + str(shape(hankelm1))+" \n" #242x2184
#
#

#T=bilstochsys.Isomorphism(hbilstoch)

#print "Isomorphism: "+array2string(T)+"\n"

T=randn(bilstochsys.state_dimension,bilstochsys.state_dimension)
T=array([[1,2,3,4],[1,2,1,2],[3,4,5,7],[1,1,1,1]],float)
bilstochsys_iso=bilstochsys.IsomorphicCopy(T)

print "Isomorphic copy\n"

bilstochsys_iso.ownprint()

print "Noise variance : "+str(bilstochsys_iso.NoiseVariance())+"\n"

#bilstochsys.checkIsomorphism(T,bilstochsys_iso)

scovariance = bilstochsys_iso.ComputeOutputSelfCovariance()

(hankelm1, msize1) = bilstochsys_iso.HankelMatrix(bilstochsys_iso.state_dimension)
#hankelm1=hankelm1[0:msize1,0:400]

hbilstoch = stoch_bilin.HankelMatrixToStochBilSys(hankelm1, bilstochsys_iso.output_dimension, bilstochsys_iso.discrete_modes, bilstochsys_iso.psigmas, scovariance, bilstochsys_iso.language,bilstochsys_iso.alphas,msize1)
#
print "Bilinear stochastic system from Hankel matrix"
hbilstoch.ownprint()



print "Hankel matrix size: " + str(msize1)+"\n"
print "Hankel matrix dimension2: " + str(shape(hankelm1))+" \n"

result=bilstochsys_iso.CoordinatedForm(1)

print "GBS in coordinated form"

bilstochsys_coord=result[0]
coordinator=result[1]

print "Coordinated system"
bilstochsys_coord.ownprint(oprecision=2)

print "Coordinator"
coordinator.ownprint(oprecision=2)

(chankelm1, cmsize1) = coordinator.HankelMatrix(coordinator.state_dimension)

#
cscovariance =coordinator.ComputeOutputSelfCovariance()
#
coordinator_min = stoch_bilin.HankelMatrixToStochBilSys(chankelm1, coordinator.output_dimension, coordinator.discrete_modes, coordinator.psigmas, cscovariance, coordinator.language,coordinator.alphas,cmsize1)

#coordinator_min = coordinator.MinimalSystem()


print "Variance of the coordinator noise: "+str(coordinator.NoiseVariance())
print "Variance of the innovation process of the output the coordinator "+str(coordinator_min.NoiseVariance())+"\n"
print "Minimal dimension of the coordinator: "+str(coordinator_min.state_dimension)+"\n"


print "Minimzed coordinated system"
coordinator_min.ownprint(oprecision=2)



print "Is coordinator isomorphic to minimal one "
T1=coordinator.Isomorphism(coordinator_min)
coordinator.checkIsomorphism(T1,coordinator_min)
#
#(xstate,youtput,u) =  bilstochsys.SimulateSystemDiscrete(K=10**6)
#
#print "Compare output distribution of the bilinear system one based on outputs/matrix formulas"
#bilstochsys.CheckEqualityOfOutputDistributions(youtput,u, N=bilstochsys.state_dimension)
#bilstochsys.CheckAssumptions(K=10**6,  simulation_type='DISC')
