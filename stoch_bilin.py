import form_pow_repr
from numpy import dot as matrixmultiply
from numpy.linalg import lstsq as linear_least_squares
from own_io import own_print
from  numpy import *
import hankel_matrix
import ConfigParser
import Gnuplot
import sys
from abstract_hankel_generate import *
from word_operations import *

#Class of stochastic bilinear systems


#Set numerical accuracy
#def_precsion = 1e-1#20
#hankel_matrix.def_precision = def_precision

#form_pow_repr.def_precision = def_precision 

def init_module(l_def_precision = 1e-10 ):
	
        def_precision = l_def_precision
	hankel_matrix.def_precision = l_def_precision

	form_pow_repr.def_precision = l_def_precision 
	return 	


#Computes a bilinear stochastic system from Hankel matrix of the formal power series Psi_y asscociated with a process y. 
#The parameters are as follows: hmatrix is the Hankel-matrix H_{Psi_y,N,N+1}, 
#output_dimension is the number of outputs, discrete_modes is the alphabet, psigmas is a dictionary such that
#psigmas[sigma]=p_sigma,  self_covariances[sigma]=T_{sigma,sigma}, language is the set S which determines the language L, length=N.

def HankelMatrixToStochBilSys(hmatrix, output_dimension, discrete_modes, \
			    psigmas, self_covariances, language,alphas,length):
	
	index = []
	for dstate in discrete_modes:
		for i in range( output_dimension ):
			index.append((dstate, i))
			
	odimension = output_dimension
	hankm = hankel_matrix.HankelMatrix(\
	                     discrete_modes, odimension,\
	                     hmatrix, index, length)
	
	repr = hankm.ComputeRepresentation(is_row=False)

	#repr.ownprint()

	bilsys = ReprToStochBilSys(repr, output_dimension, self_covariances, psigmas, language,alphas)
	
	return bilsys
	

#Solve algebraic riccati equation for bilinear stochstic systems. Returns a triple [P,Q,K], where P[sigma]=E[x(t)x^T(t)u_{sigma}^2], Q[sigma]=Q_sigma=E[e(t)e'(t)u_sigma] and K_sigma is the Kalman gain.
def SolveRiccati(a_matrices,b_matrices, c_matrices, output_dimension,self_covariance, psigmas, discrete_modes, language,number_iter=[]):

	pmatrices=dict([])
	kmatrices=dict([])
	noise_variance=dict([])

	state_dimension = c_matrices.shape[1]

        if number_iter==[]:
           number_iter=50


	for dstate in discrete_modes:
		pmatrices[dstate]      = zeros((state_dimension, state_dimension))
		kmatrices[dstate]      = sqrt(psigmas[dstate])*matrixmultiply(b_matrices[dstate],linalg.inv(psigmas[dstate]*self_covariance[dstate]))
                noise_variance[dstate] =  psigmas[dstate]*self_covariance[dstate]


	epsilon = 1e-10


	isStop = False

	iter = 0

	while  (not isStop):
			
		new_pmatrices = dict([])
		new_kmatrices = dict([])
		new_noise_variance = dict([])

		for dstate in discrete_modes:
			new_pmatrices[dstate] = zeros((state_dimension, state_dimension))
			for dstate1 in discrete_modes:
				if (dstate1,dstate) in language:	
					Noise_Part1 =   (sqrt(psigmas[dstate1])*b_matrices[dstate1]-(1/sqrt(psigmas[dstate1]))*matrixmultiply(\
                                            a_matrices[dstate1], matrixmultiply(pmatrices[dstate1],transpose(c_matrices)) ) )
					
					Noise_Part2 = matrixmultiply(Noise_Part1, matrixmultiply(linalg.pinv(noise_variance[dstate1]), transpose(Noise_Part1)))

					CovPart     = (1/psigmas[dstate1])*matrixmultiply(\
                                        	       matrixmultiply(a_matrices[dstate1],pmatrices[dstate1]), transpose(a_matrices[dstate1]))

					new_pmatrices[dstate]=new_pmatrices[dstate]+psigmas[dstate]*(CovPart + Noise_Part2)

			
			new_noise_variance[dstate] = psigmas[dstate]*self_covariance[dstate] - matrixmultiply(c_matrices,\
                                                     matrixmultiply(new_pmatrices[dstate], transpose(c_matrices)) )

			new_kmatrices[dstate] = matrixmultiply(\
                                           (sqrt(psigmas[dstate])*b_matrices[dstate]-(1/sqrt(psigmas[dstate]))*matrixmultiply(\
                                            a_matrices[dstate], matrixmultiply(new_pmatrices[dstate],transpose(c_matrices)) ) ),\
                                            linalg.pinv(new_noise_variance[dstate]))

			#print "Noise variance: " + array2string(new_noise_variance[dstate])
			#print "Old Noise variance: " + array2string(noise_variance[dstate])
			#print "Pmatrix: " + array2string(new_pmatrices[dstate])
			#print "Old Pmatrix: " + array2string(pmatrices[dstate])
			#print "Kmatrix: " + array2string(new_kmatrices[dstate])
			#print "Old Kmatrix: " + array2string(kmatrices[dstate])
			#print "Self covariance: " + array2string(self_covariance[dstate])

		max= - 1

		for dstate in discrete_modes:
			if (linalg.norm(pmatrices[dstate]-new_pmatrices[dstate]) > max):
				max = linalg.norm(pmatrices[dstate] - new_pmatrices[dstate])/(linalg.norm(pmatrices[dstate])+1)
			

		isStop = (max < epsilon) | (iter == number_iter) 
		#isStop = (iter == number_iter) 


		#for dstate in discrete_modes:
		#	print "Psigma riccati: "+array2string(pmatrices[dstate])+"\n"

		pmatrices      = new_pmatrices
		noise_variance = new_noise_variance
		kmatrices  = new_kmatrices

		iter = iter + 1

		print "Iter " + str(iter)+" max: "+str(max)+"\n"
		


	return [pmatrices,noise_variance,kmatrices]
	


 
  
		
  	
#Returns a generalized bilinear system G_R based on the representation R of the formal power series Psi_y. The parameters are as follows:
#output_dimension is the number of outputs, discrete_modes is the alphabet, psigmas is a dictionary such that
#psigmas[sigma]=p_sigma,  self_covariances[sigma]=T_{sigma,sigma}, language is the set S which determines the language L.
def ReprToStochBilSys(repr,\
                         output_dimension, self_covariance, psigmas, language,alphas):
	discrete_modes = repr.alphabet
	a_matrices = repr.transition
	ra_matrices = dict()
	c_matrices = repr.output
	b_matrices = dict([])


	
	zeta = repr.zeta
	for key in discrete_modes:
		is_empty = True
		for i in range(output_dimension):
			if is_empty:
				bmatrix =reshape(zeta.pop((key,i)),(repr.dimension,1))
				
				is_empty =False
			else:
				bmatrix = concatenate((bmatrix,\
					zeta.pop((key,i))),1)
					
		b_matrices[key] = bmatrix

	for dstate in discrete_modes:
		ra_matrices[dstate] = (1/sqrt(psigmas[dstate]))*a_matrices[dstate]

        [pmatrices,noise_variance,kmatrices] = SolveRiccati(a_matrices,b_matrices, c_matrices, \
                                                     output_dimension,self_covariance, psigmas, discrete_modes, language)

	bilsys= BilinStochSystem(discrete_modes=\
	   discrete_modes,\
	              a_matrices=ra_matrices, \
		      b_matrices= kmatrices ,\
		      c_matrices=c_matrices, \
		      d_matrices= identity(output_dimension), \
		      noise_variance = noise_variance, language = language, psigmas=psigmas, alphas=alphas)
		      
	return bilsys



#Compute $u_w (1/sqrt{p}_w)$. The process $u$ is represented as dictionary: u[sigma] represents u_sigma. A process is represented as a matrix, each column of which corresponds
#to a data point and each row of which corresponds to a variable.psigmas is a dictionary such that
#psigmas[sigma]=p_sigma,
def ComputeU(u,word,psigmas):
	
	l = len(word)
        Kp = shape(u[word[0]])
	K = Kp[0]

	uwprocess = ones((1,K-l+1))
	#uwprocess1 = ones((1,K-l+1))

	count = 0
	for symbol in word:
		v = u[symbol]
		uwprocess = uwprocess*v[count:(K-l+1)+count]*(1/sqrt(psigmas[symbol]))
		count = count + 1

	#for i in range(K-l+1):
	#	count = 0
	#	for symbol in word:
	#		v = u[symbol]
	#		uwprocess1[:,i] = uwprocess1[:,i]*v[count+i]*(1/sqrt(psigmas[symbol]))
	#		count = count + 1
	
	#print "Difference uwprocess: "+str(linalg.norm(uwprocess-uwprocess1))+"\n"

	return uwprocess
		
				


#computes z_w^x: process = x, word = w, u={u_sigma}. A process is represented as a matrix, each column of which corresponds
#to a data point and each row of which corresponds to a variable; psigmas is a dictionary such that
#psigmas[sigma]=p_sigma,
def ComputeZ(process,word,u,psigmas):

	l = len(word)
	n = shape(process)[0]
        K = shape(process)[1]	

	uwprocess = ComputeU(u,word,psigmas)


	zprocess=uwprocess*process[:,0:K-l+1]

	return zprocess
			


#computes Lambad_w = E[x(t)(z_w^x(t))^T]: process = x, word = w, u={u_sigma}. A process is represented as a matrix, each column of which corresponds 
#to a data point and each row of which corresponds to a variable; psigmas is a dictionary such that
#psigmas[sigma]=p_sigma,
def ComputeLambda(yprocess,word,u,psigmas):
	

	(p,K) = shape(yprocess)
	
	uv = dict([])

	for key in u.keys():
		v = u[key]
		uv[key]=v[0:K-1]

	

	z = ComputeZ(yprocess[:,0:K-1],word,uv,psigmas)

	l = len(word)

	yshifted = yprocess[:,l:K] 

	CovBig = cov(yshifted,z)


	Lambda = CovBig[0:p,p:2*p]
	
	return Lambda


#computes T_{w,v} = E[z^x_w(t)(z_v^x(t))^T]: process = x, wword = w, vword = v, u={u_sigma}. A process is represented as a matrix, each column of which corresponds 
#to a data point and each row of which corresponds to a variable; psigmas is a dictionary such that
#psigmas[sigma]=p_sigma.
def ComputeTZ(yprocess,wword, vword,u,psigmas):
	
	z1 = ComputeZ(yprocess,wword,u,psigmas)

	z2 = ComputeZ(yprocess,vword,u,psigmas)


	CovBig = cov(z1,z2)

	p = shape(yprocess)[0]

	T = CovBig[0:p,p:2*p]
	
	return T
 	

#Computes the values of the formal power series asscociated with y for the words from a set S: wordlist = S, youtputs = y, psigmas is a dictionary such that
#psigmas[sigma]=p_sigma. Returns a list with values.
# More precisely, the argument youtput is a 
# p \times K vector, whose ith column corresponds to the sample $y(i)$. The argument u is a dictionary, such that 
# u[sigma] is a K vector, whose ith element is the sample u_{\sigma}(i).  Then 
# \Lambda^e_w = 1/(K-|w|) \sum_{i=|w|}^{K-1} youtputs(i)youtputs'(i-|w|)u_{w}(i), where for w=sigma1...sigmak,
# u_w[i]=u[sigma1][0].... u[sigmak][i]
# Finally, \Psi(w)=[\Lambda_{\sigma_1 w},... \Lambda_{\sigma_d w}], where \Sigma={\sigma_1,..,\sigma_d}.
def FormalPowerSeriesEstimate(wordlist, youtputs,u,psigmas,alphabet):
	
	formalseries = []

	isListEmpty=True
	for word in wordlist:
		isSmallListEmpty = True
		for sigma in alphabet:
			if isSmallListEmpty:
				series_value = ComputeLambda(youtputs,[sigma]+word,u,psigmas)
				isSmallListEmpty = False

			else:
				series_value = hstack((series_value,ComputeLambda(youtputs,[sigma]+word,u,psigmas)))

		if isListEmpty:
			formalseries =  series_value
			isListEmpty  = False
		else:
			formalseries = vstack((formalseries, series_value))

	

	return formalseries


def KronProdBigA(a_matrices):
	
	alphabet = a_matrices.keys()
	n = 	shape(a_matrices[alphabet[0]])[0]

	A=zeros((n*n,n*n))

	for key in alphabet:
		A=A+kron(a_matrices[key], a_matrices[key])



	return A


#Generate a random stochastic system
def GenerateRandomBilinearSystem(state_dimension,input_dimension, output_dimension, alphabet, psigmas, alphas, language):
	

	a_matrices = dict([])
	ra_matrices = dict([])
	b_matrices = dict([])
	noise_variance = dict([])

	c_matrices = random.randn(output_dimension,state_dimension)
	d_matrices = random.randn(output_dimension,input_dimension)

	noise_variance_total = random.randn(input_dimension, input_dimension)

	noise_variance_total = dot(noise_variance_total, transpose(noise_variance_total))


	for dstate in alphabet:
		a_matrices[dstate] = random.randn(state_dimension,state_dimension)
		b_matrices[dstate] = random.randn(state_dimension, input_dimension)
		ra_matrices[dstate] = a_matrices[dstate]*sqrt(psigmas[dstate])
		noise_variance[dstate] = noise_variance_total * psigmas[dstate]		

	bigA = KronProdBigA(ra_matrices)

	[w,v] = linalg.eig(bigA)

	factor = max(abs(w))

	for dstate in alphabet:
		a_matrices[dstate] = a_matrices[dstate]*(1.0/sqrt(factor+1))

	
	return BilinStochSystem(discrete_modes=alphabet,\
	              a_matrices= a_matrices, \
		      b_matrices= b_matrices ,\
		      c_matrices=c_matrices, \
		      d_matrices= d_matrices, \
		      noise_variance = noise_variance, language = language, psigmas=psigmas, alphas=alphas)

	








#Class of generalized bilinear systems
class BilinStochSystem:

	attribute_list = ["discrete_modes", \
	                  "a_matrices", "b_matrices", \
			   "c_matrices", "d_matrices", "noise_variance", "psigmas", "language" \
			   "state_dimension", "input_dimension",\
			   "output_dimension"]
	
	constructor_attribute_list = ["discrete_modes", \
	                   "a_matrices", "b_matrices", \
			   "c_matrices", "noise_variance", "psigmas", "language", "d_matrices", "alphas" ]
			   
	def check_object(self):
		return


	def copy(self, bilsys):
		for attrib in BilinStochSystem.attribute_list:
			self.__dict__[attrib] =  bilsys.__dict__[attrib]
	
        #Constructor of the class. Can be called in the following ways
        # 1) BilinStochSystems(BilinStochSystem(discrete_modes=\
	#   discrete_modes,\
	#              a_matrices=ra_matrices, \
	#	      b_matrices= kmatrices ,\
	#	      c_matrices=c_matrices, \
	#	      d_matrices= identity(output_dimension), \
	#	      noise_variance = noise_variance, language = language, psigmas=psigmas)
        #    where a_matrices[sigma]=A_sigma, b_matrices[sigma]=K_sigma, c_matrices=C, d_matrices=D noise_variance[sigma]=Q_sigma=E[v(t)v'(t)u_{sigma}^2(t)],  
        #    psigmas[sigma]=p_sigma,  self_covariances[sigma]=T_{sigma,sigma}, language is the set S which determines the language L
        # 2) BilinStochSys(config_file=fp), where fp is the file handle of the configuration file which contains the definition of the parameters
        # a_matrices, b_matrices, c_matrices, d_matrices,noise_variance, psigmas and language described above.
	def __init__(self, **arguments):
	         #discrete_modes, a_matrices, b_matrices, \
                 #c_matrices, initial_states, is_check=True):
		if arguments.has_key("copy"):
			BilinStochSystem.copy(self,\
			  arguments["copy"])
		
		else:
			data = dict()
			if arguments.has_key("config_file"):
				configf = ConfigParser.\
				            ConfigParser()
				
				fp = arguments["config_file"]

				configf.readfp(fp)

				data_list = configf.items(\
				 "BilinStochSystem")

				for element in data_list:
					key       = element[0]
					value     = element[1]
					print "Key "+str(key)+" value "+str(value)+"\n"
					data[key] = eval(value)

			else:
			   	data = arguments
				
			
			for key in \
			 		BilinStochSystem.constructor_attribute_list:
			 		self.__dict__[key] = \
				 		 data[key]
				  
			self.state_dimension = self.a_matrices[self.discrete_modes[0]].shape[0]
			self.input_dimension = self.b_matrices[self.discrete_modes[0]].shape[1]
			self.output_dimension = self.c_matrices.shape[0]

			IsDataGood = True
			#Check sizes:
			for mode in self.discrete_modes:
				IsDataGood = IsDataGood and (shape(self.a_matrices[mode]) == (self.state_dimension,self.state_dimension))
				IsDataGood = IsDataGood and (shape(self.b_matrices[mode]) == (self.state_dimension,self.input_dimension))

			IsDataGood = IsDataGood and (shape(self.c_matrices) == (self.output_dimension,self.state_dimension))
			IsDataGood = IsDataGood and (shape(self.d_matrices) == (self.output_dimension,self.input_dimension))

			if (not IsDataGood):
				print "Ill formed bilinear system, quitting"
				sys.exit(-1)
		
		print "State dimension "+ \
		str(self.state_dimension)+\
		" output dimension "+ \
		str(self.output_dimension)+\
		" input dimension "+ \
		str(self.input_dimension)+"\n"
		
		
	#Prints the matrices of the system	
	def ownprint(self, oprecision=10, supp_small=1):
		print "Discrete modes:"+ str(self.discrete_modes) +"\n"
		for mode in self.discrete_modes:
			print "Discrete mode: "+ \
				str(mode) + "\n "+ \
				"A matrix:\n "+ \
				array2string(self.a_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n "+\
				"B matrix:\n "+\
				array2string(self.b_matrices[mode],\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n " + \
                               "psigmas: "+str(self.psigmas[mode]) +"\n " + \
                                "Noise variances: "+array2string(self.noise_variance[mode],\
                                precision = oprecision, \
                                suppress_small=supp_small) \
				+"\n " + \
                               "alphas: "+str(self.alphas[mode]) +"\n " + \
                                "Noise variances: "+array2string(self.noise_variance[mode],\
                                precision = oprecision, \
                                suppress_small=supp_small) \

		print "C matrix:\n "+\
		array2string(self.c_matrices,\
				precision = oprecision, \
				suppress_small=supp_small)\
				+"\n " + \
				"D matrix:\n "+\
				array2string(self.d_matrices,\
				precision = oprecision, \
				suppress_small=supp_small)\

		print "Language\n"

		for element in self.language:
			print " "+ str(element)+" "


	def ComputeIndexList(self):
		ret_index_set = []
			
		for state in self.discrete_modes:
			for i in range(self.output_dimension):
				ret_index_set.append((state,i))
		
		return ret_index_set
			
	

 	#Solve the lyapunov equation to compute P_sigma. If p_matrices=self.solveLyapunov(), then P_sigma=p_matrices[sigma] where $P_sigma=E[x(t)x'(t)u_{sigma}(t)]$. 
	def solveLyapunov(self):

		solution = dict([]);

		for dstate in self.discrete_modes:
			solution[dstate]=zeros((self.state_dimension,self.state_dimension))


		epsilon = 1e-10


		isStop = False

		iter = 0

		while  (not isStop):
			
			new_sol = dict([])

			for dstate in self.discrete_modes:
				new_sol[dstate] = 0
				for dstate1 in self.discrete_modes:
					if (dstate1,dstate) in self.language:
						new_sol[dstate]=new_sol[dstate]+self.psigmas[dstate]* (matrixmultiply(\
                                               		matrixmultiply(self.a_matrices[dstate1],solution[dstate1]), \
                                               		transpose(self.a_matrices[dstate1]))+matrixmultiply(\
                                               		matrixmultiply(self.b_matrices[dstate1], self.noise_variance[dstate1]),\
                                               		transpose(self.b_matrices[dstate1])))

			max= - 1

			for dstate in self.discrete_modes:
				if (linalg.norm(solution[dstate]-new_sol[dstate]) > max):
					max = linalg.norm(solution[dstate] - new_sol[dstate])
			

			isStop = (max < epsilon) 


			solution = new_sol
		
			iter = iter + 1


		#print "Iter: " + str(iter)+"\n"
		return solution
                        
		
        #Returns the representation asscociated with the system 
	def ComputeRepresentation(self):
		is_empty = True


		if self.__dict__.has_key("repr"):
			return self.repr
		


		zeta = dict()

		output=self.c_matrices

		p_matrices = self.solveLyapunov()


		for dstate in self.discrete_modes:
			print "P matrix at " + str(dstate) + array2string(p_matrices[dstate])+"\n"

		ra_matrices = dict([])

		for mode in self.discrete_modes:

			ra_matrices[mode] = sqrt(1.0*self.psigmas[mode])*self.a_matrices[mode]

			g_matrix = (1.0/sqrt(self.psigmas[mode]))*(matrixmultiply(\
                                   matrixmultiply(self.a_matrices[mode],p_matrices[mode]), transpose(self.c_matrices)) + \
                                   matrixmultiply(matrixmultiply(self.b_matrices[mode], self.noise_variance[mode]),\
                                   transpose(self.d_matrices))) 

		        for i in range(self.output_dimension):
				zeta[(mode,i)]=reshape(g_matrix[:,i], (self.state_dimension,1))
			
		self.repr = form_pow_repr.Representation(self.discrete_modes, \
	            ra_matrices, output, zeta )
	
		return self.repr

        #Returns a dictionary self_covariances[sigma]=T_{sigma,sigma}^y=1/p_{sigma} E[y(t)y'(t)u_{sigma}^2].
	def ComputeOutputSelfCovariance(self):
		p_matrices = self.solveLyapunov()

		output_cov = dict([])

		for dstate in self.discrete_modes:
			output_cov[dstate] = (1.0/(self.psigmas[dstate])) * (matrixmultiply(self.d_matrices, \
                                             matrixmultiply(self.noise_variance[dstate], transpose(self.d_matrices)))+\
                                             matrixmultiply(self.c_matrices, \
                                             matrixmultiply(p_matrices[dstate], transpose(self.c_matrices))))
 

		return output_cov


		
	#Returns the matrix A=sum_{sigma} p_{sigma} A_sigma^T \otimes A_sigma^T
	def bigA(self):

		ra_matrices = dict([])

		for dstate in self.discrete_modes:
			ra_matrices[dstate] = self.a_matrices[dstate]*sqrt(self.psigmas[dstate])
		
		return KronProdBigA(ra_matrices)

		
        #Returns true if A=sum_{sigma} p_{sigma} A_sigma^T \otimes A_sigma^T is stable.
	def IsStable(self):

		bigA = self.bigA()

		[w,v] = linalg.eig(bigA)

		print "Eiegenvalues " + array2string(w)+"\n"

		return (max(w) < 1)
     

        #Returns a minimal system which is equivalent to the original one. 
	def MinimalSystem(self):
		repr  = self.ComputeRepresentation()
		mrepr = repr.MinimalRepresentation()
		return ReprToStochBilSys(mrepr, self.output_dimension, self.ComputeOutputSelfCovariance(), self.psigmas,self.language, self.alphas )

        
        #Returns true if the system is observable
 	def IsObservable(self):
		repr = self.ComputeRepresentation()
		return repr.IsObservable()

        #Returns true if the system is reachable
	def IsReachable(self):
		repr = self.ComputeRepresentation()
		return repr.IsReachable()


	def NoiseVariance(self):
		result=0
		
		for dstate in self.discrete_modes:
			result=result + self.alphas[dstate]*self.alphas[dstate]*trace(self.noise_variance[dstate])


		return result

 
        def IsomorphicCopy(self,T):
		print "T :" + str(shape(T))+"\n"
		rc_matrices=matrixmultiply(self.c_matrices,linalg.inv(T));
		ra_matrices=dict([])
		rk_matrices=dict([])

		for dstate in self.discrete_modes:
			ra_matrices[dstate]=matrixmultiply(T, matrixmultiply(self.a_matrices[dstate],linalg.inv(T)))
			rk_matrices[dstate]=matrixmultiply(T,self.b_matrices[dstate])

		return BilinStochSystem(discrete_modes=\
	   self.discrete_modes,\
	              a_matrices=ra_matrices, \
		      b_matrices= rk_matrices ,\
		      c_matrices=rc_matrices, \
		      d_matrices= self.d_matrices, \
		      noise_variance = self.noise_variance, language = self.language, psigmas=self.psigmas, alphas=self.alphas)
            

	def CoordinatedForm(self,np2):
		omaps=dict()
		for i in range(np2):	
			omaps[i]=zeros((self.state_dimension,self.state_dimension))
	 		omaps[i][0:self.state_dimension,:] = reshape(self.c_matrices[i+self.output_dimension-np2,:], (self.state_dimension,1))
		
			otrans = dict()

		for dstate in self.discrete_modes:
			otrans[dstate] =transpose(self.a_matrices[dstate])

		t_obs_matrix = form_pow_repr.compute_recursive(omaps, otrans)

 		r=t_obs_matrix.shape[1]

                print "r: "+str(r)+"\n"

                w=eye(self.state_dimension) - dot(t_obs_matrix, transpose(t_obs_matrix))

                svd_decomp = linalg.svd(w)

                print "S: "+array2string(svd_decomp[1])+"U: "+array2string(svd_decomp[0])+"VT: "+array2string(svd_decomp[2])+"\n"

                w = svd_decomp[0]
		w=w[:,0:self.state_dimension-r]

		print "t_obs_matrix "+ array2string(t_obs_matrix)+"\n"
		print "w "+ array2string(w)+"\n"

                T=hstack((w,t_obs_matrix))

                print "Is orthogonal: "+array2string(dot(transpose(T),T))+"\n"

                coordinated_system=self.IsomorphicCopy(linalg.inv(T))


		ra_matrices=dict()
		rk_matrices=dict()
		rnoise = dict([])
		for dstate in self.discrete_modes:
			ra_matrices[dstate]=coordinated_system.a_matrices[dstate][self.state_dimension-r:self.state_dimension, self.state_dimension-r:self.state_dimension]
			rk_matrices[dstate]=coordinated_system.b_matrices[dstate][self.state_dimension-r:self.state_dimension, self.output_dimension-np2:self.output_dimension]
			rnoise[dstate]=coordinated_system.noise_variance[dstate][self.output_dimension-np2:self.output_dimension, self.output_dimension-np2:self.output_dimension]

			

		coordinator = BilinStochSystem(discrete_modes=\
	      self.discrete_modes,\
	              a_matrices=ra_matrices, \
		      b_matrices= rk_matrices ,\
		      c_matrices=coordinated_system.c_matrices[self.output_dimension-np2:self.output_dimension,self.state_dimension-r:self.state_dimension], \
		      d_matrices= coordinated_system.d_matrices[self.output_dimension-np2:self.output_dimension,self.output_dimension-np2:self.output_dimension], \
		      noise_variance = rnoise, language = self.language, psigmas=self.psigmas, alphas=self.alphas)


                
		return [coordinated_system,coordinator]
                

                

		
		



		
	def Isomorphism(self,bil2):

		omaps = dict()
		for i in range(self.output_dimension):	
			omaps[i]=zeros((self.state_dimension+bil2.state_dimension,1))
	 		omaps[i][0:self.state_dimension,:] = reshape(self.c_matrices[i,:], (self.state_dimension,1))
	 		omaps[i][self.state_dimension:self.state_dimension+bil2.state_dimension,:] = reshape(bil2.c_matrices[i,:], (bil2.state_dimension,1))
		
		otrans = dict()

		for dstate in self.discrete_modes:
			otrans[dstate] =zeros((self.state_dimension+bil2.state_dimension,self.state_dimension+bil2.state_dimension))
			otrans[dstate][0:self.state_dimension,0:self.state_dimension]=transpose(self.a_matrices[dstate])
			otrans[dstate][self.state_dimension:self.state_dimension+bil2.state_dimension,self.state_dimension:self.state_dimension+bil2.state_dimension]=transpose(bil2.a_matrices[dstate])
		
		t_obs_matrix = form_pow_repr.compute_recursive( omaps, otrans )

		print "t_obs_matrix "+ array2string(t_obs_matrix)+"\n"



		U1=t_obs_matrix[0:self.state_dimension,:]
		U2=t_obs_matrix[self.state_dimension:self.state_dimension+bil2.state_dimension,:]
                print "U1 "+array2string(U1)+ "\n U2 "+array2string(U2)+"\n"
		Tret = linear_least_squares(transpose(U2), transpose(U1))
		T=Tret[0]
		#T = transpose(T)

	

		return T
		
	 

        def checkIsomorphism(self,T,bil2):

		for mode in self.discrete_modes:
				print "Discrete mode: "+ \
				str(mode) + "\n "+ \
				"A matrix difference:\n "+ \
				array2string(dot(T,self.a_matrices[mode])-dot(bil2.a_matrices[mode],T))\
				+"\n "+\
				"B matrix difference:\n "+\
				array2string(dot(T,self.b_matrices[mode])-bil2.b_matrices[mode])+"\n"

		print "C matrix:\n "+\
		array2string(self.c_matrices-dot(bil2.c_matrices,T))

			
		


        #Returns the hankel matrix H_{Psi_y,N,N+1} of the formal power series  Psi_y asscociated with the output of the system.        # If N is not specified, then returns H_{Psi_y,K,K+1} such that rank H_{Psi_y,K,K} equals the dimension of a minimal
        # representation of Psi.  If [hnkm,hkmsize]=HankelMatrix(), then H_{\Psi_y,N,N+1}=hnkm and hkmsize=N if N was specified or hnkm=H_{Psi_y,K,K+1} and hmksize=K
	def HankelMatrix(self,N=[]):
		repr = self.ComputeRepresentation()
		#mrepr=repr.MinimalRepresentation()
                #mdim = mrepr.dimension

		ind_list_repr = self.ComputeIndexList()
		print "Index set: "+str(ind_list_repr)+"\n"
		hnkm = repr.HankelSubMatrix(\
		                             N,N+1,ind_list_repr)
		
		return (hnkm,N)
	 	
         #Returns the list \Psi(w), |w| le N of the values of the formal power series asscociated with the output of the system
	def FormalPowerSeries(self, N):
		repr =BilinStochSystem.ComputeRepresentation(self)
		index_list_repr = self.ComputeIndexList()
		
		wordset = WordGenerate( repr.alphabet, N )

		formal_list = []

		Bmatrix = []
		isEmptyB = True
		for index in index_list_repr:
			if isEmptyB:
				Bmatrix=repr.zeta[index]
				isEmptyB = False
			else:
				Bmatrix=hstack((Bmatrix, repr.zeta[index]))
		
		isListEmpty=True
		for word  in wordset:
			series_value = dot(self.c_matrices, dot(repr.ComputeMatrixProduct(word),Bmatrix))

			if isListEmpty:
				formal_list =  series_value
				isListEmpty = False
			else:
				formal_list=vstack((formal_list, series_value))
					
		return (formal_list,wordset)


	def SimulateSystemDiscreteMarkov(self,K=100000):
		
		u = dict([])

		random.seed()

		epsilon = 1e-4

		pseq = []

		for dstate in self.discrete_modes:
			pseq.append(self.psigmas[dstate])


		if abs(1-sum(pseq)) > epsilon:
			print "The sum of p_sigmas should be 1"
			sys.exit(-1) 


		uvalues = random.multinomial(1,pseq,size=K)

		for ind in range(len(self.discrete_modes)):
			u[self.discrete_modes[ind]] = uvalues[:,ind]
			#print "Estimated mean " + str(mean(uvalues[:,ind]))+" psigma:"+str(self.psigmas[self.discrete_modes[ind]])+"\n"
	


		return self.SimulateSystemWithU(u,K)


	#Simulates the GBS where the probability distribution of u_{\sigma}
        #is assumed to be discrete, i.e. u_{\sigma}(t) takes value 0 or 1 and
        # Prob(u_{sigma}(t)=1)=p_{sigma}, and u_{\sigma}(t) is an I.I.D
        # process, u_{\sigma_1}(t), u_{\sigma_2}(t) are independent,
        # the sigma algebra generated by \{u_{\sigma}(t)\}_{\sigma, t} is
        # indepent of the noise process. Here K is the length of simulation.
        # The function returns a tuple (x,y,u), where:
        # the ith column of x corresponds to x(i), the ith column of
        # y corresponds to the output y(i) at time i. The corresponding
        # inputs are encoded as follows: u[\sigma] is an array whose ith
        # element is u_{\sigma}(i). The simulation works as follows.
        # First, the inputs u_{\sigma} are generated. Then, the 
        # variance of the noise process is computed, by using
        # Q = Q_{\sigma}\alpha_{\sigma}^2. The algoritm works only
        # if $Q_{\sigma}=p_{\sigma} Q$. With this computed variance,
        # a sampled path of the noise process is generated, by drawing mK samples from a Gaussian distribution with zero mean and
        # covariance Q. The resulting time series is then used for simulation.
	def SimulateSystemDiscrete(self,K=100000):
		
		u = dict([])

		random.seed()

		epsilon = 1e-4

		pseq = []
		for dstate in self.discrete_modes:
			pseq.append(self.psigmas[dstate])


		if abs(1-sum(pseq)) > epsilon:
			print "The sum of p_sigmas should be 1"
			sys.exit(-1) 


		uvalues = random.multinomial(1,pseq,size=K)

		for ind in range(len(self.discrete_modes)):
			u[self.discrete_modes[ind]] = uvalues[:,ind]
			#print "Estimated mean " + str(mean(uvalues[:,ind]))+" psigma:"+str(self.psigmas[self.discrete_modes[ind]])+"\n"
	


		return self.SimulateSystemWithU(u,K)


        #Simulates the GBS where for each \sigma, $u_{\sigma}(t)$ is either
        # deterministic and equals sqrt{p_{\sigma}}, or 
        # u_{\sigma}(t) is an I.I.D.  Gaussian processes with 
        # zero mean with
        # variance p_{\sigma}. The 
        # processes, u_{\sigma_1}(t), u_{\sigma_2}(t) are independent,
        # the sigma algebra generated by \{u_{\sigma}(t)\}_{\sigma, t} is
        # indepent of the noise process. 
        # Here K is the length of simulation.
        # The function returns a tuple (x,y,u), where:
        # the ith column of x corresponds to x(i), the ith column of
        # y corresponds to the output y(i) at time i.
        # The corresponding
        # inputs are encoded as follows: u[\sigma] is an array whose ith
        # element is u_{\sigma}(i). The simulation works as follows.
        # First, the inputs u_{\sigma} are generated. Then, the
        # variance of the noise process is computed, by using
        # Q = Q_{\sigma}\alpha_{\sigma}^2. The algoritm works only
        # if $Q_{\sigma}=p_{\sigma} Q_{\sigma}$. With this computed variance,
        # the sampled values of the noise are generated with a random
        # number generator using, by taking $K$ samples
        #  from a Gaussian distribution with zero mean and
        # variance $Q$.  With this computed variance,
        # a sampled path of the noise process is generated, by drawing mK samples from a Gaussian distribution with zero mean and
        # covariance Q. The resulting time series is then used for simulation.       
 
	def SimulateSystemGauss(self,K=10000, input_type=[]):
		
		if input_type==[]:
			input_type=dict([])
			for mode in self.discrete_modes:
				input_type[mode] = 'GAUSS'

		
		u = dict([])

		for dstate in self.discrete_modes:
			if input_type[dstate] == 'GAUSS':
				random.seed()
				u[dstate] = random.randn(K)*sqrt(self.psigmas[dstate])
			
			if input_type[dstate] == 'DET':
				u[dstate] = ones(K)*sqrt(self.psigmas[dstate])
			
			
		return self.SimulateSystemWithU(u,K)


  
	#Simulates the GBS where $u_{\sigma}(t)$ is defined
        # The corresponding inputs are encoded as follows: 
        # u[\sigma] is an array whose ith element is a sample u_{\sigma}(i). 
        # The function returns a tuple (x,y), where:
        # the ith column of x corresponds to x(i), the ith column of
        # y corresponds to the output y(i) at time i. 
        # The
        # variance of the noise process is computed, by using
        # Q = Q_{\sigma}\alpha_{\sigma}^2. The algoritm works only
        # if $Q_{\sigma}=p_{\sigma} Q_{\sigma}$.
        # With this computed variance,
        # a sampled path of the noise process is generated, by drawing mK samples from a Gaussian distribution with zero mean and
        # covariance Q. The resulting time series is then used for simulation.

	def SimulateSystemWithU(self, u, K=10000):
		
		epsilon = 0.01
		
		noise = dict([])

		noisevartotal = 0
	

		#random.seed(len(self.discrete_modes)+1)
		random.seed()

		count = 0
		for dstate in self.discrete_modes:
				noisevartotal=noisevartotal+self.noise_variance[dstate]*(self.alphas[dstate]*self.alphas[dstate])
			

		for dstate in self.discrete_modes:
				if ((linalg.norm(noisevartotal*self.psigmas[dstate] - self.noise_variance[dstate])/linalg.norm(self.noise_variance[dstate])) > epsilon):
					print "Invalid noise covariances: Q_{sigma}=p_{sigma} Q should hold \n"
					print "Relative error: " + str((linalg.norm(noisevartotal*self.psigmas[dstate] - self.noise_variance[dstate])/linalg.norm(self.noise_variance[dstate])))+"\n"
					sys.exit(-1)


		L = linalg.cholesky(noisevartotal)


		#noise = dot(transpose(L), random.randn(self.input_dimension,K))
		
		noise = transpose(random.multivariate_normal(zeros((self.input_dimension,)), noisevartotal, K))

		print "Noise shape: " + str(shape(noise))+"\n"
		
		variance_est = cov(noise)
		m_est = mean(noise)

		print "Noisevatotal:" + array2string(noisevartotal)+"\n"
		print "L:" + array2string(L)+"\n"
		print "Noisevatotal est:" + array2string(variance_est)+"\n"
		print "Noisevatotal mean:" + str(m_est)+"\n"
		

		print "Sizer "+ str(shape(noise))+"\n"

	
		xf = zeros((self.state_dimension, 1))
  
                xstates = zeros((self.state_dimension,K))
		
		youtputs = zeros((self.output_dimension,K))

		for i in range(K):
			
			xstates[:,i] = reshape(xf, (self.state_dimension,))

			xfn = zeros((self.state_dimension,1))

			for dstate in self.discrete_modes:
				vv = u[dstate]
				xfn = xfn+(dot(self.a_matrices[dstate],xf))*vv[i]
				xfn = xfn  + \
					(dot(self.b_matrices[dstate],\
				reshape(noise[:,i],(self.input_dimension,1))))*vv[i]



			y = dot(self.c_matrices,xf) + dot(self.d_matrices,reshape(noise[:,i], (self.input_dimension,1)))

			youtputs[:,i] = reshape(y, (self.output_dimension,))

			xf = xfn
  			#print "Shape xf" + str(shape(xfn))+" \n"

		return (xstates,youtputs,u)
			 	



			
        #The method compute the set {\Psi(w)\}_{|w| \le N}, where $\Psi$ is the formal power series assocociated with the output y of the GBS.
        #The only input parameter is N, where N is the maximal length of the words w, for which the formal power series are computed.
        #The method returns two variables (formal_list, wordset). The variable formal_list is a p \times Kp matrix, where K is the number of words of
        #length at most N. The variable wordset is a list of size K, each element of which is a list of symbols representing a word. If the ith element of
        # wordset represents the word w, then the ith p \times p block of formal_list equals \Psi(w). 		
	def FormalPowerSeries2(self,p_matrices, N):
		repr =BilinStochSystem.ComputeRepresentation(self)
		index_list_repr = self.ComputeIndexList()
		
		wordset = WordGenerate( repr.alphabet, N )

		formal_list = []

		Bmatrix = []
		isEmptyB = True
		for mode in self.discrete_modes:
		
			g_matrix = (1.0/sqrt(self.psigmas[mode]))*(matrixmultiply(\
                                   matrixmultiply(self.a_matrices[mode],p_matrices[mode]), transpose(self.c_matrices)) + \
                                   matrixmultiply(matrixmultiply(self.b_matrices[mode], self.noise_variance[mode]),\
                                   transpose(self.d_matrices))) 
     
			if isEmptyB:
				Bmatrix=g_matrix
				isEmptyB = False
			else:
				Bmatrix=hstack((Bmatrix, g_matrix))
		

		isListEmpty=True
		for word  in wordset:
			series_value = dot(self.c_matrices, dot(repr.ComputeMatrixProduct(word),Bmatrix))

			if isListEmpty:
				formal_list =  series_value
				isListEmpty = False
			else:
				formal_list=vstack((formal_list, series_value))
					
		return (formal_list,wordset)




        #This method checks if the covariances of the GBS computed from the formulas coincide with the covariances of the output process estimated 
        #using the output time series (argument youtputs) and input time series (argument u). More precisely, using the corresponding formulas involving
        #the system matrices, \Lamda_{w}, w \in \Sigma^{+}, |w| \le N and T_{\sigma,\sigma}, \sigma \in \Sigma are computed. 
        #Using the time series $youtputs$ and $u$, the estimates
        # \Lambda_w^e, w \in \Sigma^{+}, T_{\sigma,\sigma}^e, \sigma \in \Sigma are computed. Finally,  the vectors
        # [\Lambda^e_w-\Lambda_w | w \in \Sigma^{+}, |w| le N]  are computed and the following errors are displayed:
        #   -- absolute error: ||[\Lambda^e_w-\Lambda_w | w \in \Sigma^{+}, |w| le N]||_2  
        #   -- relative error: ||[\Lambda^e_w-\Lambda_w | w \in \Sigma^{+}, |w| le N])||_2/||[\Lambda_w | w \in \Sigma^{+}, |w| le N])||_2
        #   -- maximal error: ||[\Lambda^e_w-\Lambda_w | w \in \Sigma^{+}, |w| le N])||_{infty}
        # In addiyion, the maximal relative error max_{\sigma \in \Sigma} (||T_{\sigma,\sigma}^e - T_{\sigma,\sigma}||_2/||T_{\sigma,\sigma}||_2) is computed
        # and displayed.
        # The estimation of \Lambda_w^e, w \in \Sigma^{+}, T_{\sigma,\sigma}^e, \sigma \in \Sigma is done as follows. The argument youtput is a 
        # p \times K vector, whose ith column corresponds to the sample $y(i)$. The argument u is a dictionary, such that 
        # u[sigma] is a K vector, whose ith element is the sample u_{\sigma}(i).  Then 
        # \Lambda^e_w = 1/(K-|w|) \sum_{i=|w|}^{K-1} youtputs(i)youtputs'(i-|w|)u_{w}(i), where for w=sigma1...sigmak,
        # u_w[i]=u[sigma1][0].... u[sigmak][i]
        # T^e_{sigma,sigma} = 1/(K-2) \sum_{i=2}^{K-1} youtputs(i)youtputs(i-1)u[sigma](i) 
	def CheckEqualityOfOutputDistributions(self,youtputs,u, N=-1):

		if (N < 0):
			N = self.state_dimension

		
		(form1,wordlist) = self.FormalPowerSeries(N)

		form2 = FormalPowerSeriesEstimate(wordlist, youtputs,u,self.psigmas,self.discrete_modes)

			
		output_list=[]

		difference = form1 - form2

	 	print "Relative difference in formal power series: " + str((linalg.norm(difference,2)/linalg.norm(form1,2))*100)+"%\n"
		print "Absolute difference in formal power series: " + str(linalg.norm(difference,2))+"\n"
		print "Maximal difference in formal power series: "   + str( amax(difference.flatten()) ) +"\n"

		cov1 = self.ComputeOutputSelfCovariance()

		max= -1


		for dstate in self.discrete_modes:
			cov2 = ComputeTZ(youtputs,[dstate],[dstate],u,self.psigmas)

			error = linalg.norm(cov2-cov1[dstate],2)/linalg.norm(cov2,2)

			if (error > max):
				max = error


		print "Maximal relative difference between output self-covariances:" + str(max*100)+"%\n"




		
	# The purpose of the method below is to test if the standard assumptions for GBSs are satisfied for the GBS at hand. More precisely, 
        # we check the following assumptions:  
        # 1) are the formal power series of the representation associated with the GBS is the same as the formal power series
        #     which is estimated from simulation data.
        # 2) are the covariances T_{\sigma,\sigma} as computed from the system matrices are the same as the covariances estimated from the simulation
        #    data.
        # The  relative differences between these objects are displayed. 
        # The agruments of the method are as follows:
        #  K -- number of simulation steps
        #  simulation_type -- if equals the string 'GAUSS', then the inputs are either deterministic or Gaussian
        #                     otherwise, the inputs are assumed to be discrete valued
        # input_type -- for each sigma, if input_type[sigma]='GAUSS', then the process u_{sigma}(t) is taken to be Gaussian, 
        #                and if input_type[sigma]='DET', then the process u_{sigma}(t) is taken to be deterministic. See the description of
        #                SimulateSystemGauss. Note that input_type is used only if simulation_type='GAUSS'
	# makeGraph --if True, the time series youtput is plotted.
        # The method works as follows: it simulates the GBS using SimulateSystemGauss (if simulation_type = 'GAUSS') or using SimulateSystemDiscrete (otherwise) and
        # uses the thus computed input and output time series to estimate the formal power series asscociated with the output of the GBS. It computes
        # the same formal power series from the system matrices. Then the results are compared. 
	def CheckAssumptions(self, K=100000,  simulation_type="GAUSS", input_type=[], makeGraph=False):
		
		if simulation_type == "GAUSS":
			(xstates,youtputs,u) = self.SimulateSystemGauss(K, input_type)
		else:
			(xstates,youtputs,u) = self.SimulateSystemDiscrete(K)
		
		Pestt = dict([])

		P =  self.solveLyapunov()

		#print "Shape y "+str(shape(youtputs))+"\n"

		Pest = dict([])
		for dstate in self.discrete_modes:
			Pestt[dstate] = ComputeTZ(xstates,[dstate],[dstate],u,self.psigmas)*self.psigmas[dstate]
				
			#Pest[dstate]=zeros((self.state_dimension,self.state_dimension))

			#v = u[dstate]
			#for i in range(K):
			#	xst = reshape(xstates[:,i],(self.state_dimension,1))
			#	Pest[dstate] = Pest[dstate]+(1.0/K)*dot(xst*v[i], transpose(xst)*v[i])



		vmax=-1
		for dstate in self.discrete_modes:
			serror = linalg.norm(P[dstate]-Pestt[dstate])/linalg.norm(P[dstate])

			if (serror > vmax):
				vmax=serror

		print "Difference in state covariances2: " + str(vmax)+"\n"

		#N =self.state_dimension
		N = 4

		(form1,wordlist) = self.FormalPowerSeries(N)

		form2 = FormalPowerSeriesEstimate(wordlist, youtputs,u,self.psigmas,self.discrete_modes)
		(form3,wordlist) = self.FormalPowerSeries2(Pestt,N)

			

		if makeGraph:
			output_list=[]

			for i in range(K):
				output_list.append(youtputs[:,i])
		
			g=Gnuplot.Gnuplot(debug=1)
			g.plot(output_list)
			raw_input('Please press return to continue...\n')

		print "Difference in formal power series: " + str(linalg.norm((form1-form2)/linalg.norm(form1))*100)+"%\n"
		print "Difference in formal power seriesV2: " + str(linalg.norm((form1-form3)/linalg.norm(form1))*100)+"%\n"


		cov1 = self.ComputeOutputSelfCovariance()

		max= -1


		for dstate in self.discrete_modes:
			cov2 = ComputeTZ(youtputs,[dstate],[dstate],u,self.psigmas)

			error = linalg.norm(cov2-cov1[dstate])/linalg.norm(cov2)

			if (error > max):
				max = error


		print "Maximal relative difference between output self-covariances:" + str(max*100)+"%\n"




		


		return
		
